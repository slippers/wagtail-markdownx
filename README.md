# wagtail-markdownx

This project implements the django-markdownx field as a wagtail block.
Includes a tabbed admin editor interface to maximize workspace.

## Screenshots

![edit](./docs/wagtail-markdownx-edit.png)


![preview](./docs/wagtail-markdownx-preview.png)

# Setup


## Development setup

This package is not on pypi.  Install locally and import into your wagtail project.

    git clone https://gitlab.com/slippers/wagtail-markdownx.git

    pip install -e [wagtail-markdownx location] 

## Add to INSTALLED_APPS in this order

in your config INSTALLED_APPS

    'wagtail_markdownx',
    'markdownx',

## django-markdonwx extra

in your config.
    
    MARKDOWNX_MARKDOWN_EXTENSIONS = ['markdown.extensions.extra',]

## Reference in your wagtail model

    from wagtail_markdownx.blocks.markdown_block import MarkdownBlock

## Issues

### MarkdownX not exported

django-markdownx has a javascript file that does not export the MarkdownX object to window.

[export MarkdownX](https://github.com/neutronX/django-markdownx/issues/166)

workaround is to include a copy of their js output in the wagtail-markdownx package

### Testing

Looking for a pattern to follow.
