


window.markdownx_apply = function(id){
    var containerId = id + '-markdownx-container';
    var container = document.getElementById(containerId);

    var tabcontent = container.getElementsByClassName("tabcontent");
    var tablinks = container.getElementsByClassName("tablinks");

    var editor  = container.querySelector('.markdownx-editor');
    var preview = container.querySelector('.markdownx-preview');

    const openTab = function(ev){
        for (i = 0; i < tabcontent.length; i++) {
            if(this.name === tabcontent[i].id){
                tabcontent[i].style.display = "block";
            } else {
                tabcontent[i].style.display = "none";
            }
        }
        // Get all elements with class="tablinks" and remove the class "active"
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        this.className += " active";
    }

    tabcontent.Edit.style.display = "block";
    tabcontent.Preview.style.display = "none";
    tablinks.Edit.className += " active";

    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].addEventListener('click', openTab, false);
    }


    // Only add the new MarkdownX instance to
    // fields that have no MarkdownX instance yet.
    if ( !editor.hasAttribute('data-markdownx-init') ) {
        return new MarkdownX(container, editor, preview)
    }

}
