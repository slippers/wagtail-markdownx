from django.utils.translation import ugettext_lazy as _
from django.forms import Media
from django.utils.functional import cached_property
from wagtail.admin.staticfiles import versioned_static
from wagtail.core.blocks import FieldBlock, StreamBlock
from markdownx.fields import MarkdownxFormField
from wagtail.core.blocks.field_block import FieldBlockAdapter

from wagtail.core.telepath import register
from wagtail.core.widget_adapters import WidgetAdapter


class MarkdownBlock(FieldBlock):

    class Meta:
        help_text = 'Markdown Block'
        icon = 'media'
        template = 'wagtail_markdownx/markdown_block.html'
        form_classname = 'markdown-block'

    def __init__(self, required=True, help_text=None, max_length=None, min_length=None, validators=(), *args, **kwargs):
        self._required = required
        self._help_text = _('Mardown text.') if help_text else help_text
        self._validators = validators
        super().__init__(*args, **kwargs)

    @cached_property
    def widget(self):
        from markdownx.widgets import AdminMarkdownxWidget
        return AdminMarkdownxWidget()

    @cached_property
    def field(self):
        return MarkdownxFormField(
            required=self._required,
            help_text=self._help_text,
            max_length=None,
            min_length=None,
            validators=self._validators,
            widget=self.widget,
        )



class MarkdownBlockAdapter(FieldBlockAdapter):
    
    class Media:
        js=[
            versioned_static('wagtail_markdownx/js/markdownx_util.js'),
        ]
        
        css={
            'all': [versioned_static('wagtail_markdownx/css/markdownx_editor.css')]
        }


register(MarkdownBlockAdapter(), MarkdownBlock)
