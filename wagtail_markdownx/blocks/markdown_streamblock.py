from wagtail.core.blocks import FieldBlock, StreamBlock
from .markdown_block import MarkdownBlock


class MarkdownStreamBlock(StreamBlock):

    markdown = MarkdownBlock()
    
    class Meta:
        icon='cogs'
